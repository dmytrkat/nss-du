# NSS DU

## Authors
- Jasmine Smith
- Willow Jones

## Technologies Used
- JavaScript
- HTML
- CSS
- jQuery

## Description
This browser application allows users to create vegetable, flower, and fruit gardens. Users can create a grid and then block out sections of the grid for each plant. Simply enter the dimensions of your garden in the handy form, then add custom shapes and labels for your plants!

## Setup/Installation Requirements 
- Clone this repository to your desktop.
- Navigate to the top level of the directory. 
- Open `js/index.html` in your browser.

## Known Bugs
- The tomato icon is currently broken

## Copyright
© 2024 Jasmine Smith and Willow Jones. All rights reserved.